#!/usr/bin/env bash

# tmux configuration
wget \
    --output-document \
    "${HOME}"/.tmux.conf \
    https://gitlab.com/-/snippets/2100018/raw/master/.tmux.conf

# vim configuration
wget \
    --output-document \
    "${HOME}"/.vimrc \
    https://raw.githubusercontent.com/amix/vimrc/master/vimrcs/basic.vim